class AddIncomeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :income, :decimal, default: 0
  end
end

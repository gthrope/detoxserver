class ChangeInputType < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.change :inputs, :text
    end
  end
end

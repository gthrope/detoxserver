class RemoveIncomeFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :income
  end
end

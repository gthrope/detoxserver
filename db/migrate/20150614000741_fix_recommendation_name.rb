class FixRecommendationName < ActiveRecord::Migration
  def change
    rename_column :users, :recommendation, :recommendations
  end
end

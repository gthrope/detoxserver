class QuestionsController < ApplicationController

  before_action :logged_in_user
  before_action :admin_user

  def index
    respond_to do |format|
      format.json { render json: @questions } #need?
      format.html
    end
  end

  def updateAll

  end

end

class UsersController < ApplicationController
  before_action :logged_in_user, except: [:new, :create]
  before_action :correct_user_or_admin, only: [:show, :edit, :update]
  before_action :admin_user, only: [:index, :destroy]
  wrap_parameters User, include: [:name, :email, :password, :password_confirmation, :inputs, :recommendations]

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      log_in @user
      respond_to do |format|
        format.json { render json: @user, status: 200 }
        format.html { redirect_to @user }
      end
    else
      render plain: @user.errors.full_messages[0], status: 400
    end
  end

  def show
    respond_to do |format|
      format.json { render json: @user, status: 200 }
      format.html { render "show" }
    end
  end

  def getCurrentUser
    @user = current_user
    show
  end

  def index
    @users = User.all
  end

  def destroy
    User.find(params[:id]).destroy
    redirect_to users_url
  end

  def edit
  end

  def updateInputs

    @user = current_user

    if(params[:user].has_key?(:inputs))
      oldInputsHash = @user.inputs ? eval(@user.inputs) : {}
      newInputsHash = params[:user][:inputs]
      params[:user][:inputs] = oldInputsHash.merge(newInputsHash).to_s
    end

    update

  end

  def update
    if @user.update_attributes(user_params)
      respond_to do |format|
        format.json { render json: @user, status: 200 }
        format.html { redirect_to @user }
      end
    else
      respond_to do |format|
        format.json { render plain: @user.errors.full_messages, status: 400 }
        format.html { render plain: @user.errors.full_messages, status: 400 }
      end
    end
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation, :inputs, :recommendations)
    end

    def correct_user_or_admin
      @user = User.find(params[:id])
      render plain: "Access denied.", status: 400 unless current_user?(@user) || current_user.admin?
    end

end

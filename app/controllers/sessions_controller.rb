class SessionsController < ApplicationController

  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      log_in user
      respond_to do |format|
        format.json { render json: user, status: 200 }
        format.html { render json: user, status: 200 } # redirect_to users_path
      end
    else
      render plain: "Invalid email/password combination", status: 400
    end
  end

  def destroy
    log_out if logged_in?
    respond_to do |format|
      format.json { render nothing: true, status: 200 }
      format.html { redirect_to sessions_path }
    end
  end

end

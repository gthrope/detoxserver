class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception
  include SessionsHelper

  private

    def logged_in_user
      unless logged_in?
        respond_to do |format|
          format.json { render plain: "Access denied. Please log in.", status: 400 }
          format.html { redirect_to sessions_path }
        end
      end
    end

    def admin_user
      render plain: "Access denied. Please request administrator access to view\
 this page.", status: 400 unless current_user.admin?
    end

end

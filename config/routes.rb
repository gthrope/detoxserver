Rails.application.routes.draw do

  resources :users
  post 'updateInputs' => 'users#updateInputs'
  get 'getCurrentUser' => 'users#getCurrentUser'
  root 'users#index'

  get 'sessions' => 'sessions#new'
  post 'sessions' => 'sessions#create'
  delete 'sessions' => 'sessions#destroy'

#  get 'questions' => 'questions#index'
#  post 'questions' => 'questions#updateAll'

end
